from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from .models import *

# Create your tests here.
class TestViews(TestCase):
    def test_index_url_exists(self):
        response = Client().get(reverse('index'))
        self.assertEquals(response.status_code, 200)
    
    def test_index_using_index_template(self):
        response = Client().get(reverse('index'))
        self.assertTemplateUsed(response, 'index.html')

    def test_index_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)