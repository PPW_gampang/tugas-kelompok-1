from django.urls import path
from .views import *

urlpatterns = [
    path('', article, name='article'),
    path('<str:nama_judul>/', article_details, name='article_details')
]