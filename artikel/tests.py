from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from .models import *

# Create your tests here.
class TestViews(TestCase):
    def setUp(self):
        artikel1 = Artikel.objects.create(
            judul = 'artikel1',
        )

    def test_article_url_exists(self):
        response = Client().get(reverse('article'))
        self.assertEquals(response.status_code, 200)
    
    def test_article_using_article_template(self):
        response = Client().get(reverse('article'))
        self.assertTemplateUsed(response, 'articles.html')

    def test_article_using_article_function(self):
        found = resolve('/article/')
        self.assertEqual(found.func, article)

    def test_rent_form_url_exists(self):
        response = Client().get(reverse('article_details', args=['artikel1']))
        self.assertEquals(response.status_code, 200)



class TestModels(TestCase):
    def test_artikel_model_str_method(self):
        a = Artikel(judul="yay")
        self.assertEqual(str(a), "yay")