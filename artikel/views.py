from django.shortcuts import render
from .models import *

def article(request):
    judul = Artikel.objects.all()
    return render(request, "articles.html", {"judul": judul})

def article_details(request, nama_judul):
    judul_artikel = Artikel.objects.all()
    title = Artikel.objects.get(judul=nama_judul)
    return render(request, 'articles_details.html', {'title': title})