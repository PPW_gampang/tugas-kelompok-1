from django.db import models

# Create your models here.
class Artikel(models.Model):
    judul = models.CharField(max_length=255, default="")
    tanggalArtikel = models.DateField(auto_now_add=True)
    descArtikel = models.TextField(max_length=4096, default="")
    gambar = models.FileField(null=True, blank=True)

    def __str__(self):
        return self.judul

