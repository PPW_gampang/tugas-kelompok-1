from django import forms
from . import models


class TransaksiForm(forms.ModelForm):
    class Meta:
        model = models.Transaksi
        fields = [
            "penyewa",
            "lamaSewa",
            
        ]
        labels = {
            "penyewa": "Nama",
            "lamaSewa": "Lama Sewa (hari)"
        }
        

class UlasanForm(forms.ModelForm):
    class Meta:
        model = models.Ulasan
        fields = [
            "username",
            "pesan",
            "rating",
        ]
        labels = {
            "username": "Username",
            "pesan": "Pesan",
            "rating": "Rating"
        }
        

class MobilForm(forms.ModelForm):
    class Meta:
        model = models.Mobil
        fields = [
            "name",
            "username",
            "desc",
            "tahun",
            "kota",
            "price",
            "image",
        ]
        labels = {
            "name": "Nama Lengkap",
            "username": "Username",
            "desc": "Deskripsi Mobil",
            "tahun": "Tahun Keluaran Mobil",
            "kota": "Lokasi",
            "price": "Harga Sewa (hari)"
        }
