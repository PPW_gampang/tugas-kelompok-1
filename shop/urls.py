from django.urls import path
from .views import *


urlpatterns = [
    path('', shop, name='shop'),
    path('details/<str:carname>', details, name='details'),
    path('daftar/', daftar, name='daftar'),
    path('rent_form/<str:carname>', rent_form, name="rent_form"),
    path('review_form/<str:carname>', review_form, name="review_form"),
    path('api/get_cars_by_query/<str:query>', get_cars_by_query, name="get_cars_by_query"),
]