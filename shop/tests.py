from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import *
from .views import *

# Create your tests here.
class TestViews(TestCase):
    def setUp(self):
        car1 = Mobil.objects.create(
            name='car1',
            username='uname1',
        )

    def test_shop_url_exists(self):
        response = Client().get(reverse('shop'))
        self.assertEquals(response.status_code, 200)
    
    def test_shop_using_shop_template(self):
        response = Client().get(reverse('shop'))
        self.assertTemplateUsed(response, 'shop.html')

    def test_shop_using_shop_function(self):
        found = resolve('/shop/')
        self.assertEqual(found.func, shop)

    def test_daftar_url_exists(self):
        response = Client().get(reverse('daftar'))
        self.assertEquals(response.status_code, 200)
    
    def test_daftar_using_daftar_template(self):
        response = Client().get(reverse('daftar'))
        self.assertTemplateUsed(response, 'daftar.html')

    def test_daftar_using_daftar_function(self):
        found = resolve('/shop/daftar/')
        self.assertEqual(found.func, daftar)

    def test_details_url_exists(self):
        response = Client().get(reverse('details', args=['car1']))
        self.assertEquals(response.status_code, 200)

    def test_review_form_url_exists(self):
        response = Client().get(reverse('review_form', args=['car1']))
        self.assertEquals(response.status_code, 200)

    def test_rent_form_url_exists(self):
        response = Client().get(reverse('rent_form', args=['car1']))
        self.assertEquals(response.status_code, 200)

    # def test_review_form_POST_save_data(self):
    #     response = Client().post(reverse(''))
    #     self.assertEquals(response.status_code, 302)
    #     self.assertEquals(self.)



class TestModels(TestCase):
    def test_mobil_model_str_method(self):
        a = Mobil(name="yay")
        self.assertEqual(str(a), "yay")

    def test_ulasan_model_str_method(self):
        a = Ulasan(username="yay")
        self.assertEqual(str(a), "yay")

    def test_transaksi_model_str_method(self):
        a = Transaksi(penyewa="yay")
        self.assertEqual(str(a), "yay")