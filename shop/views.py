from django.shortcuts import render, redirect
from django.utils import timezone
from .models import *
from .forms import *
from .serializers import MobilSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response

# Create your views here.
def shop(request):
    cars = Mobil.objects.all()
    return render(request, 'shop.html', {'cars' : cars})


def details(request, carname):
    # mobil = Mobil.objects.all()
    target = Mobil.objects.get(name=carname)
    image = target.image
    review = Ulasan.objects.filter(mobil__name=carname)
    transaksi = Transaksi.objects.filter(mobilSewa__name=carname)
    return render(request, 'details.html', {'review': review, 'transaksi': transaksi, 'target': target, 'image':image})


def daftar(request):
    response = {
        'mobil_form': MobilForm(),
        'status':False
    }
    if request.method == 'POST':
        mobil_form = MobilForm(request.POST)
        if mobil_form.is_valid():
            data = mobil_form.cleaned_data
            if data.get('tahun') > 0:
                mobil_form.save()
                return render(request, 'daftar.html', {'mobil_form':MobilForm(), 'status': 'Sukses Didaftarkan'})
            else:
                return render(request, 'daftar.html', {'mobil_form':MobilForm(), 'status': 'Terjadi kesalahan'})
    return render(request, 'daftar.html', response)


def rent_form(request, carname):
    if (request.method=="POST"):
        form = TransaksiForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.mobilSewa = Mobil.objects.get(name=carname)
            post.tanggal = timezone.now()
            post.save()
            return redirect('shop')

    form = TransaksiForm()
    return render(request, 'rent_form.html', {'form': form})


def review_form(request, carname):
    if (request.method=="POST"):
        form = UlasanForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.mobil = Mobil.objects.get(name=carname)
            post.date = timezone.now()
            post.save()
            return redirect('shop')
    
    form = UlasanForm()
    return render(request, 'review_form.html', {'form': form})

def suggestion(request):
    return render(request, 'suggestion.html')

@api_view(['GET'])
def get_cars_by_query(request, query):
    cars = []
    if query.isnumeric():
        cars = Mobil.objects.filter(price__range = (0, int(query)))
    else:
        cars = Mobil.objects.filter(name__icontains = query)
    serializer = MobilSerializer(cars, many=True)
    return Response(serializer.data)