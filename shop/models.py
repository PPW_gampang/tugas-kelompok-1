from django.db import models
from django.db.models.fields.files import ImageField

class Mobil(models.Model):
    name = models.CharField(max_length=255, default="", unique=True)
    username = models.CharField(max_length=255, default="")
    desc = models.TextField(max_length=1028, default="")
    tahun = models.IntegerField(default=0)
    kota = models.CharField(max_length=255, default="")
    price = models.CharField(max_length=255, default="")
    image = models.FileField(null=True,blank=True, upload_to='media/')
    # tipe = models.ForeignKey(Kategori, )

    def __str__(self):
        return self.name

class Ulasan(models.Model):
    username = models.CharField(max_length=255, default="")
    mobil = models.ForeignKey(Mobil, on_delete=models.CASCADE)
    pesan = models.TextField(max_length=2056, default="")
    date = models.DateField(auto_now_add=True)
    rating = models.FloatField()

    def __str__(self):
        return self.username

class Transaksi(models.Model):
    penyewa = models.CharField(max_length=255, blank=False)
    mobilSewa = models.ForeignKey(Mobil, on_delete=models.CASCADE)
    lamaSewa = models.IntegerField()
    tanggal = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.penyewa