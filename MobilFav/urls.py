from django.urls import path

from .views import *

app_name = 'MobilFav'

urlpatterns = [
    path('', wish, name='wish'),
    path('add/<str:carname>/', wish_add, name='wish_add'),
    path('item_clear/<str:carname>/', item_clear, name='wish_clear'),
]
