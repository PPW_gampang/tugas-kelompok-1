from django.test import TestCase, Client
from django.http import request
from django.urls import resolve
from django.conf import settings
from .views import *
from django.shortcuts import reverse
from shop.models import Mobil
from .models import MobilFavorit
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from cart.cart import Cart


# Create your tests here.
class UnitTest(TestCase):
    def setUp(self):
        super().setUp()
        self.user = User.objects.create_user(username="handi", email="handi@gmail.com", password="handi")
        self.user.save()
        self.client.force_login(self.user)

        mobil1 = Mobil.objects.create(name="joko", username ="joko123", price="Rp 500.000", image="nanana")
        mobil1.save()
        wishlist = MobilFavorit(mobil=mobil1)
        wishlist.save()

    def test_wish_url_is_exist(self):
        response = self.client.get('/wishlist/')
        self.assertEqual(response.status_code, 200)

    def test_wish_using_template(self):
        response = self.client.get('/wishlist/')
        self.assertTemplateUsed(response, 'wishlist.html')

    def test_wish_func(self):
        found = resolve('/wishlist/')
        self.assertEqual(found.func, wish)

    def test_wish_model_create_new_object(self):
        self.assertEqual(MobilFavorit.objects.all().count(), 1)

    def test_add_url_is_redirecting(self):
        response = self.client.get(reverse('MobilFav:wish_add', kwargs={'carname':'joko'}))
        self.assertEqual(response.status_code, 302)

    def test_item_clear_url_is_redirecting(self):
        response = self.client.get(reverse('MobilFav:wish_clear', kwargs={'carname':'joko'}))
        self.assertEqual(response.status_code, 302)

