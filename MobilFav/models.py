from __future__ import unicode_literals

from django.db import models

from shop.models import Mobil


class MobilFavorit(models.Model):
    mobil = models.ForeignKey(Mobil, on_delete=models.SET_NULL, null=True)
    date_added = models.DateTimeField(auto_now=True)


