from __future__ import unicode_literals
from django.shortcuts import render, redirect
from .models import MobilFavorit
from cart.cart import Cart
from django.contrib.auth.decorators import login_required
from shop.models import Mobil

# @login_required(login_url="/otentikasi/login/")
def wish_add(request, carname):
    cart = Cart(request)
    mobil = Mobil.objects.get(name=carname)
    if mobil.image is None:
        mobil.image = "/static/1.jpg"
    cart.add(product=mobil)
    try:
        mobil1 = MobilFavorit.objects.get(mobil=mobil)
    except MobilFavorit.DoesNotExist:
        mobil_favorit = MobilFavorit.objects.create(mobil=mobil)
        mobil_favorit.save()
    return redirect("/shop/details/"+carname)


# @login_required(login_url="/otentikasi/login/")
def item_clear(request, carname):
    cart = Cart(request)
    product = Mobil.objects.get(name=carname)
    cart.remove(product)
    mobil_favorit = MobilFavorit.objects.get(mobil=product)
    mobil_favorit.delete()
    return redirect("MobilFav:wish")

# @login_required(login_url="/otentikasi/login/")
def wish(request):
    mobil_favorit = MobilFavorit.objects.all()
    if len(mobil_favorit) == 0:
        cart = Cart(request)
        cart.clear()
    return render(request, 'wishlist.html')
    
